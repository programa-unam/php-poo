<?php
//creación de la clase carro
class Carro2{
	//declaracion de propiedades
	public $color;
	public $modelo;
	public $circula;
	public $year;

	//declaracion del método verificación
	public function verificacion(){
		if($this->year <1990)
		{
			$this->circula = "No";
		}else if($this->year <= 2010){
			$this->circula = "Revisión";
		}else{
			$this->circula = "Si";
		}
	}
	
}

//creación de instancia a la clase Carro
$Carro1 = new Carro2();

if (!empty($_POST)){
	$Carro1->color=$_POST['color'];
	$Carro1->modelo=$_POST['modelo'];
	$Carro1->year=$_POST['year'];
	$Carro1->verificacion();
}




