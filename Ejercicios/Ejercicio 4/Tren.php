<?php  
include_once('transporte.php');

	//declaracion de la clase hijo o subclase Carro
	class tren extends transporte{

		private $numero_vagones;

		//declaracion de constructor
		public function __construct($nom,$vel,$com,$pue){
			//sobreescritura de constructor de la clase padre
			parent::__construct($nom,$vel,$com);
			$this->numero_vagones=$pue;
				
		}

		// declaracion de metodo
		public function resumenTren(){
			// sobreescribitura de metodo crear_ficha en la clse padre
			$mensaje=parent::crear_ficha();
			$mensaje.='<tr>
						<td>Numero de vagones:</td>
						<td>'. $this->numero_vagones.'</td>				
					</tr>';
			return $mensaje;
		}
	} 

?>
